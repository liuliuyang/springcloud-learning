package com.ly.eurekaClient.controller;

import com.ly.eurekaClient.service.CallHelloService;
import com.ly.eurekaClient.service.FeignHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/get")
public class getUserController {

    @Autowired
    CallHelloService callHelloService;

    @Autowired
    FeignHelloService feignHelloService;

    @RequestMapping("/getUser")
    public String getUser(){

       return callHelloService.getUser();
    }

    @RequestMapping("/feign/getUser")
    public String geFeigntUser(){

        return feignHelloService.getUser();
    }
}
