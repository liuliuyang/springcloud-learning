package com.ly.eurekaClient.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("DOMEAPP")
public interface FeignHelloService {

    @RequestMapping("/hello/getUser")
    String getUser();
}
