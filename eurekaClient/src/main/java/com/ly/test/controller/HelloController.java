package com.ly.test.controller;

import com.ly.test.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyang
 */
@RestController
@RequestMapping("/hello")
public class HelloController {


    @RequestMapping(value = "/getUser",produces = {"application/json"})
    @ResponseBody
    public User getUser(){
        User user = new User(1L,"马蓉","真北路地铁站");
        System.out.println(user);
        return user;
    }
}
