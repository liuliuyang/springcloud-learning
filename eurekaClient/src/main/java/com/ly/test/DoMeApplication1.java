package com.ly.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * @author liuyang
 */
@SpringBootApplication
@EnableDiscoveryClient
public class DoMeApplication1 {

    public static void main (String[] args) {
        SpringApplication.run(DoMeApplication1.class,args);
    }
}
