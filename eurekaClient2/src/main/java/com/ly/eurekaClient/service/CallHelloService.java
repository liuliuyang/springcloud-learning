package com.ly.eurekaClient.service;

import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CallHelloService {

    @Value("${app.service-url}")
    private String appServiceUrl;
    @Autowired
    RestTemplate restTemplate;

    public String getUser(){
        ResponseEntity<String> result = restTemplate.getForEntity(appServiceUrl+"/hello/getUser",String.class);
        return result.getBody();
    }

}
